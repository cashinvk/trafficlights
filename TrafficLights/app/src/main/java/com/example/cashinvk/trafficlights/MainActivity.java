package com.example.cashinvk.trafficlights;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

/**
 * Класс демонстрирует работу приложения, имитируещего работу светофора
 * (смена цвета фона активити при нажатии на кнопку с цветом: красным, желтым или зеленым).
 * Предусматривается сохранение выбранного фона при изменении ориентации экрана
 *
 * @author Кашин В.О. гр.12ОИТ18к
 */
public class MainActivity extends Activity {

    private static final String ARG_KEY_COLOR_BACKGROUND = "colorBackground";

    private RelativeLayout background;

    private Button setRedBg;
    private Button setYellowBg;
    private Button setGreenBg;

    private int color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        background = (RelativeLayout)findViewById(R.id.background);

        setRedBg = (Button)findViewById(R.id.butRed);
        setYellowBg = (Button)findViewById(R.id.butYellow);
        setGreenBg = (Button)findViewById(R.id.butGreen);

        if(savedInstanceState != null){
            background.setBackgroundColor(color = savedInstanceState.getInt(ARG_KEY_COLOR_BACKGROUND));
        }

        setRedBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBackgroundActivity(setRedBg);
            }
        });

        setYellowBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBackgroundActivity(setYellowBg);
            }
        });

        setGreenBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBackgroundActivity(setGreenBg);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle saveInstanceState){
        saveInstanceState.putInt(ARG_KEY_COLOR_BACKGROUND, color);
        super.onSaveInstanceState(saveInstanceState);
    }

    /**
     * Функция производит изменение цвета фона
     * в зависимости от нажатой кнопки (красная, желтая или зеленая)
     *
     * @param buttonSetColor - нажатая кнопка
     */
    public void setBackgroundActivity(Button buttonSetColor){
        background.setBackgroundColor(color = buttonSetColor.getCurrentTextColor());
    }
}